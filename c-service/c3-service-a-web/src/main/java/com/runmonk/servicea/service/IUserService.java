package com.runmonk.servicea.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.runmonk.servicea.domain.User;

/**
 * @Description
 * @Author RunMonk
 * @Date 2024/7/22 12:02
 **/
public interface IUserService extends IService<User> {
}
