package com.runmonk.servicea;


import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@MapperScan(basePackages = {"com.runmonk.**.mapper"})
@SpringBootApplication(scanBasePackages = {"com.runmonk"})
public class TestApplication {

    public static void main(String[] args) {

        SpringApplication.run(TestApplication.class, args);
        log.info("服务A启动成功");
    }

}
