package com.runmonk.servicea.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusPropertiesCustomizer;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.core.incrementer.IKeyGenerator;
import com.baomidou.mybatisplus.extension.incrementer.OracleKeyGenerator;
import com.baomidou.mybatisplus.extension.incrementer.PostgreKeyGenerator;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @name：MybatisPlusConfig
 * @since：2021/8/9 3:03 下午
 * @author：runmonk
 */

@Configuration
@Slf4j
public class MybatisPlusConfig {
    /**
     * 数据库类型
     */
    @Value("${cn.hrfax.dbType:MYSQL}")
    private DbType dbType;

    /**
     * 新的分页插件,一缓和二缓遵循mybatis的规则,需要设置 MybatisConfiguration#useDeprecatedExecutor = false 避免缓存出现问题(该属性会在旧插件移除后一同移除)
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        //配置MybatisPlus分页插件，注意是ORACLE12以下版本
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(dbType));
        return interceptor;
    }


    /**
     * Sequence主键自增
     * mysql的设置自增：alter table TEST modify ID int primary key auto_increment;
     * oracle设置seq序列：CREATE SEQUENCE SEQ_TEST INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE CACHE 10;
     *
     * @return 返回oracle自增类，如果是mysql数据库不需要增加此内容
     */
    @Bean
    public MybatisPlusPropertiesCustomizer plusPropertiesCustomizer() {
        return plusProperties -> {
            val dbConfig = plusProperties.getGlobalConfig().getDbConfig();
            if (dbType == DbType.ORACLE) {
                List<IKeyGenerator> keyGeneratorList = new ArrayList<>();
                keyGeneratorList.add(new OracleKeyGenerator());
                dbConfig.setKeyGenerators(keyGeneratorList);
                dbConfig.setIdType(IdType.ASSIGN_ID);
            } else if (dbType == DbType.POSTGRE_SQL) {
                List<IKeyGenerator> keyGeneratorList = new ArrayList<>();
                keyGeneratorList.add(new PostgreKeyGenerator());
                dbConfig.setKeyGenerators(keyGeneratorList);
            } else if (dbType == DbType.MYSQL) {
                dbConfig.setIdType(IdType.AUTO);
            }
        };
    }

    /**
     * 设置新增和更新字段自动填充策略
     *
     * @return
     */
    @Bean
    public MetaObjectHandler metaObjectHandler() {
        return new MetaObjectHandler() {

            @Override
            public void insertFill(MetaObject metaObject) {
                this.setFieldValByName("createTime", LocalDateTime.now(), metaObject);
                this.setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
                this.setFieldValByName("lastUpdateTime", LocalDateTime.now(), metaObject);
            }

            @Override
            public void updateFill(MetaObject metaObject) {
                this.setFieldValByName("lastUpdateTime", LocalDateTime.now(), metaObject);
            }
        };
    }
}
