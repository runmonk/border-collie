//package com.runmonk.servicea.exportExcel;
//
//import com.runmonk.core.util.ExcelUtil;
//import com.runmonk.enums.TestExportExcelEnum;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.core.io.ClassPathResource;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 基于模板的 excel导出demo
// *
// * @author ：runmonk
// * @date ：Created in 2019/11/15 15:41
// */
//@RestController
//public class ExportExcelTemplateTest {
//    private static final Logger LOGGER = LoggerFactory.getLogger(ExportExcelTemplateTest.class);
//
//    /**
//     * 基于注解的excel导出
//     *
//     * @param response
//     */
//    @RequestMapping(value = "/templateExport", method = RequestMethod.GET)
//    public void exportExcel(HttpServletResponse response) throws IOException {
//        long start = System.currentTimeMillis();
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("date", new Date());
//        List<Map<String, String>> listMap = new ArrayList<Map<String, String>>();
//        for (int i = 0; i < 4; i++) {
//            Map<String, String> lm = new HashMap<String, String>();
//            lm.put("id", i + 1 + "");
//            lm.put("name", "测试模板导出");
//            lm.put("age", "12");
//            lm.put("money", "9999");
//            listMap.add(lm);
//        }
//        map.put("maplist", listMap);
//
//        LOGGER.debug("导出excel所花时间：" + (System.currentTimeMillis() - start));
//        TestExportExcelEnum excelEnum = TestExportExcelEnum.EXPORT_TEMPLATE_DETAIL;
//        ExcelUtil.exportTemplateExcel(map, excelEnum, response);
//    }
//
//    @RequestMapping(value = "/test")
//    public void test(HttpServletResponse response) throws IOException {
//        ClassPathResource classPathResource = new ClassPathResource("template/excel/test.xls");
//     InputStream initialStream = classPathResource.getInputStream();
//        HSSFWorkbook tpWorkbook = new HSSFWorkbook(initialStream);
//        System.out.println(tpWorkbook);
//    }
//
//
//
//}
