package com.runmonk.servicea.controller;

import com.runmonk.servicea.domain.User;
import com.runmonk.servicea.service.impl.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 这是一个数据源测试控制类
 */
@RestController
@Slf4j
public class DataSourceController {
//
//    @Autowired
//    private DruidDataSource dataSource;

    @Autowired
    private UserService userService;

//    /**
//     * 测试数据源链接
//     *
//     * @return
//     */
//    @RequestMapping("/dataSource")
//    public String dataSource() {
//
//        try {
//
//            System.out.println("dataSource = " + dataSource);
//            Connection conn = dataSource.getConnection();
//            System.out.println("conn = " + conn);
//            return "success";
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return "end.";
//
//    }

    @RequestMapping("/insert")
    public void insert() {
        User user = new User();
        user.setAge(20);
        user.setName("测试姓名");
        userService.add(user);
        log.info("------------》》》》》》》》》测试插入成功！");
    }


}
