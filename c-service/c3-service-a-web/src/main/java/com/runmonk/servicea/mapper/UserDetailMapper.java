package com.runmonk.servicea.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.runmonk.servicea.domain.UserDetail;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDetailMapper extends BaseMapper<UserDetail> {

}