//package com.runmonk.servicea.config;
//
//import com.runmonk.servicea.database.MybatisHelper;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.mybatis.spring.SqlSessionTemplate;
//import org.mybatis.spring.annotation.MapperScan;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.jdbc.datasource.DataSourceTransactionManager;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//import org.springframework.transaction.annotation.TransactionManagementConfigurer;
//
//import javax.sql.DataSource;
//
//
///**
// * Springbean配置
// * 类似于过去spring的application.xml
// *
// * @author runmonk
// *
// * @since 2019-09-18
// */
//@Configuration
//@EnableTransactionManagement
//@MapperScan(basePackages = {"com.runmonk.servicea.mapper"}) //mybatis mapper 所在package
//public class ServiceASpringBeansConfig implements TransactionManagementConfigurer {
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceASpringBeansConfig.class);
//
//    private static final String TYPE_ALIASES_PACKAGE = "com.runmonk.servicea.domain";
//
//    /**
//     * 数据源
//     */
//    @Autowired
//    private DataSource dataSource;
//
//
//    /**
//     * 事务
//     */
//    @Bean
//    @Override
//    public PlatformTransactionManager annotationDrivenTransactionManager() {
//        return new DataSourceTransactionManager(dataSource);
//    }
//
//    @Bean
//    public SqlSessionFactory sqlSessionFactory() {
//        return MybatisHelper.getSqlSessionFactory(dataSource, TYPE_ALIASES_PACKAGE, "classpath*:mapper/*Mapper.xml");
//    }
//
//    @Bean
//    public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
//        return new SqlSessionTemplate(sqlSessionFactory);
//    }
//
//
//}
