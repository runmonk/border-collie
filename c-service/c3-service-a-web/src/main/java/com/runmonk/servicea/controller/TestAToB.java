package com.runmonk.servicea.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestAToB {

    @RequestMapping("/feign_client")
    public String aToB() {
        return "其他服务调用A服务成功";
    }

}
