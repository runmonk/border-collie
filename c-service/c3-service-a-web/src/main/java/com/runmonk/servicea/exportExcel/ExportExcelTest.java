//package com.runmonk.servicea.exportExcel;
//
//import com.runmonk.domian.vo.PersonExportVo;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 基于注解的 excel导出demo
// *
// * @author ：runmonk
// * @date ：Created in 2019/11/15 15:41
// */
//@RestController
//public class ExportExcelTest {
//    private static final Logger LOGGER = LoggerFactory.getLogger(ExportExcelTest.class);
//
//    /**
//     * 导出
//     *
//     * @param response
//     */
//    @RequestMapping(value = "/export", method = RequestMethod.GET)
//    public void exportExcel(HttpServletResponse response, HttpServletRequest request) throws IOException {
//
//        //表格标题属性设置
//
//        List<PersonExportVo> list = new ArrayList<PersonExportVo>();
//        for (int i = 0; i < 100; i++) {
//            PersonExportVo personExportVo = new PersonExportVo();
//            personExportVo.setName("姓名" + i);
//            personExportVo.setUsername("username" + i);
//            personExportVo.setPhoneNumber("136****008" + i);
//
//            list.add(personExportVo);
//        }
//        ExcelUtils.exportExcel(list, PersonExportVo.class, "测试文件导出", "大数据测试", "测试文件导出", request, response);
//
//
//    }
//}
