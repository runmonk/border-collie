package com.runmonk.servicea.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.runmonk.servicea.domain.User;
import com.runmonk.servicea.mapper.UserMapper;
import com.runmonk.servicea.service.IUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 业务管理
 */
@Service
public class UserService extends ServiceImpl<UserMapper, User> implements IUserService {


    /**
     * 添加
     */
    @Transactional
    public void add(User user) {
        baseMapper.insert(user);
    }

    /**
     * 更新
     */
    public void update(User user) {
        baseMapper.insert(user);
    }


}