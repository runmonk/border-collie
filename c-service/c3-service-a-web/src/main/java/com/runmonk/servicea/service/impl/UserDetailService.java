package com.runmonk.servicea.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.runmonk.servicea.domain.UserDetail;
import com.runmonk.servicea.mapper.UserDetailMapper;
import com.runmonk.servicea.service.IUserDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 业务管理
 */
@Service
public class UserDetailService extends ServiceImpl<UserDetailMapper, UserDetail> implements IUserDetailService {



    /**
     * 添加
     */
    @Transactional
    public void add(UserDetail userDetail) {
        baseMapper.insert(userDetail);
    }

    /**
     * 更新
     */
    public void update(UserDetail userDetail) {
        baseMapper.insert(userDetail);
    }


}