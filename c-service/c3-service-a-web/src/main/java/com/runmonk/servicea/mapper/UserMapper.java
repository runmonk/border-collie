package com.runmonk.servicea.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.runmonk.servicea.domain.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {

}