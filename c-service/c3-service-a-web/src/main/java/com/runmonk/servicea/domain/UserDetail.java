package com.runmonk.servicea.domain;


import java.io.Serializable;

/**
 * 实体类
 */
public class UserDetail implements Serializable {
    private static final long serialVersionUID = 15683835082302L;


    private Integer id;//
    private String address;//家庭住址
    private String phone;//手机号
    private String email;//邮箱地址


    public UserDetail() {
    }

    /**
     * @param id --
     */
    public UserDetail(Integer id) {
        this.id = id;
    }

    /***/
    public Integer getId() {
        return id;
    }

    /***/
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 家庭住址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 家庭住址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 手机号
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 手机号
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 邮箱地址
     */
    public String getEmail() {
        return email;
    }

    /**
     * 邮箱地址
     */
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "UserDetail [ id=" + id + ", address=" + address + ", phone=" + phone + ", email=" + email + "]";
    }
}
