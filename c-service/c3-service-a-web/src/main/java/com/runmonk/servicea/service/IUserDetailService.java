package com.runmonk.servicea.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.runmonk.servicea.domain.UserDetail;

/**
 * @Description
 * @Author RunMonk
 * @Date 2024/7/22 12:01
 **/
public interface IUserDetailService extends IService<UserDetail> {
}
