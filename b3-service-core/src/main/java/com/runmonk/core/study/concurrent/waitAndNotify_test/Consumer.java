package com.runmonk.core.study.concurrent.waitAndNotify_test;

import java.util.List;


public class Consumer implements Runnable {

    private final List<Integer> taskQueue;


    public Consumer(List<Integer> sharedQueue) {
        this.taskQueue = sharedQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                synchronized (taskQueue) {
                    while (taskQueue.isEmpty()) {
                        System.out.println("Queue is empty " + Thread.currentThread().getName() + " is waiting , size: " + taskQueue.size());
                        taskQueue.wait();
                    }
                    Thread.sleep(2000);
                    int i = (Integer) taskQueue.remove(0);
                    System.out.println("Consumed: 获取锁资源开始消费------>>>" + i);
                    taskQueue.notifyAll();
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
