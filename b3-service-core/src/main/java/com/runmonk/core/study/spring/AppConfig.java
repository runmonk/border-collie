package com.runmonk.core.study.spring;

import org.springframework.context.annotation.ComponentScan;

/**
 * @name：AppConfig
 * @since：2021/4/25 7:05 下午
 * @author：runmonk
 */
@ComponentScan
public class AppConfig {

}
