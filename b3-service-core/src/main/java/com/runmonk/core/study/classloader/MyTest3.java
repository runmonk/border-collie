package com.runmonk.core.study.classloader;

import java.util.UUID;

/**
 * @name：MyTest3
 * @since：2019/11/25 10:22 PM
 * @author：runmonk
 */
public class MyTest3 {
    public static void main(String[] args) {
        System.out.println(MyParent3.str);
    }
}

class  MyParent3{
    public static final String str= UUID.randomUUID().toString();

    static {
        System.out.println("MyParent3 static code");
    }
}