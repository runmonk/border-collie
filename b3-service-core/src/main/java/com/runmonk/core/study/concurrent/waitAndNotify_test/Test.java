package com.runmonk.core.study.concurrent.waitAndNotify_test;


import java.util.ArrayList;
import java.util.List;

/**
 * 【线程间的通信】
 * 这是一个测试wait() 和notify()的测试类
 *
 * 这三个方法由于需要控制对对象的控制权(monitor)，所以属于Object而不是属于线程。
 * wait(),会把持有该对象线程的对象控制权交出去，然后处于等待状态。
 * notify()，会通知某个正在等待这个对象的控制权的线程可以继续运行。
 * nofifyAll(),会通知所有等待这个对象控制权的线程继续运行，如果有多个正在等待该对象控制权时，具体唤醒哪个线程，就由操作系统进行调度。
 *
 * 注意：
 * 1.生产者，消费者必须要对同一份资源进行操作。
 * 2.无论是执行对象的wait、notify还是notifyAll方法，必须保证当前运行的线程取得了该对象的控制权（monitor）
 *
 *
 * 测试说明：
 * 1、消费者线程启动，但是这时集合为空。消费者开始等待并释放锁，生产者线程启动开始往队列插入数字
 *
 * 2、插入的时候开始 生产者开始通知消费者线程，但是消费者不一定能获取锁的资源
 *
 * 3、消费者获取了锁的资源，开始消费集合中的数据，并通知生产者线程。
 *
 * 4、wait()执行后会立即释放锁，但是执行notify后不会立即释放锁，需要等线程执行完后续操作。
 *
 * @author qijin
 *
 * @since 2019-09-23
 */
public class Test {

    public static void main(String[] args) throws InterruptedException {
        //共享资源
        List<Integer> taskQueue = new ArrayList<Integer>();

        int MAX_CAPACITY = 30;
        //创建生产者线程
        Thread producer = new Thread(new Producer(taskQueue,MAX_CAPACITY),"producer");

        //创建消费者线程
        Thread consumer = new Thread(new Consumer(taskQueue),"consumer");

        consumer.start();
        Thread.sleep(2000);
        producer.start();

    }
}
