//package com.runmonk.core.study.poiDemo;
//
//import org.apache.poi.hssf.usermodel.HSSFCell;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.util.CellRangeAddress;
//import org.springframework.core.io.ClassPathResource;
//
//import java.io.FileOutputStream;
//import java.io.InputStream;
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
///**
// * @name：poiExportExcelTest
// * @since：2019/12/3 8:42 PM
// * @author：runmonk
// */
//public class ExportExcelTest {
//
//    public static void main(String[] args) {
//        FileOutputStream out;
//        try {
//            ClassPathResource classPathResource = new ClassPathResource("template/excel/test.xls");
//            InputStream initialStream = classPathResource.getInputStream();
//            HSSFWorkbook tpWorkbook = new HSSFWorkbook(initialStream);
//            // 新建一个Excel的工作空间
//            HSSFWorkbook workbook = new HSSFWorkbook();
//            // 把模板复制到新建的Excel
//            workbook = tpWorkbook;
//            Sheet sheet = workbook.getSheetAt(0);
//
//            //准备模拟数据
//            List<Order> orderList = new ArrayList<>();
//            List<OrderDetail> orderDetailList = new ArrayList<>();
//            List<OrderDetail> orderDetailList2 = new ArrayList<>();
//            for (int i = 1; i < 3; i++) {
//                OrderDetail orderDetail = new OrderDetail();
//                orderDetail.setMoney(new BigDecimal(5555 + i));
//                orderDetail.setTime(new Date());
//                orderDetailList.add(orderDetail);
//            }
//
//            for (int i = 1; i < 5; i++) {
//                OrderDetail orderDetail = new OrderDetail();
//                orderDetail.setMoney(new BigDecimal(5555 + i));
//                orderDetail.setTime(new Date());
//                orderDetailList2.add(orderDetail);
//            }
//
//            for (int i = 0; i < 4; i++) {
//                Order order = new Order();
//                order.setIndex(i + 1);
//
//                if (i == 1) {
//                    order.setFlag(true);
//                    order.setMoney(new BigDecimal(2222));
//
//                } else {
//                    order.setFlag(false);
//                    if (i == 3) {
//                        order.setOrderDetailList(orderDetailList2);
//                    } else {
//                        order.setOrderDetailList(orderDetailList);
//                    }
//                }
//                order.setTime(new Date());
//                order.setInterestRate(BigDecimal.ONE);
//                order.setName("测试1");
//                order.setMoney(new BigDecimal(1111));
//                orderList.add(order);
//            }
//            //在相应的单元格进行赋值
//            Cell cell0 = sheet.getRow(1).getCell(0);
//            cell0.setCellValue("时间：2019-11-17");
//            int length = 0;
//            int startRow = 4;
//            for (int i = 0; i < orderList.size(); i++) {
//                Order order = orderList.get(i);
//
//                if (order.getFlag()) {
//                    Cell cell1 = sheet.getRow(startRow).getCell(0);
//                    Cell cell2 = sheet.getRow(startRow).getCell(1);
//                    Cell cell3 = sheet.getRow(startRow).getCell(2);
//                    Cell cell4 = sheet.getRow(startRow).getCell(3);
//                    Row row = sheet.getRow(startRow);
//                    Cell cell5 = row.getCell(4);
//                    if (cell5 == null) {
//                        cell5 = row.createCell(4);
//                    }
//                    cell1.setCellValue("序号" + i);
//                    cell2.setCellValue("动态列1" + i);
//                    cell3.setCellValue("年龄" + i);
//                    cell4.setCellValue("动态列2" + i);
//                    cell5.setCellValue("资产");
//                    startRow = ++startRow;
//                } else {
//                    List<OrderDetail> list = order.getOrderDetailList();
//                    //起始行
//                    CellRangeAddress cellRangeAddress = new CellRangeAddress(startRow, startRow + list.size() - 1, 0, 0);
//                    CellRangeAddress cellRangeAddress2 = new CellRangeAddress(startRow, startRow + list.size() - 1, 2, 2);
//                    CellRangeAddress cellRangeAddress3 = new CellRangeAddress(startRow, startRow + list.size() - 1, 4, 4);
//                    sheet.addMergedRegion(cellRangeAddress);
//                    sheet.addMergedRegion(cellRangeAddress2);
//                    sheet.addMergedRegion(cellRangeAddress3);
//                    List<OrderDetail> orderDetailList1 = order.getOrderDetailList();
//                    for (int j = 0; j < orderDetailList1.size(); j++) {
//                        Cell cell1 = sheet.getRow(startRow).getCell(0);
//                        cell1.setCellValue("序号" + i + j);
//                        Cell cell2 = sheet.getRow(startRow + j).getCell(1);
//                        if (j == 0) {
//                            cell2.setCellValue("动态列1" + j);
//                        } else {
//                            cell2.setCellValue("动态列1222");
//                        }
//
//                        Cell cell3 = sheet.getRow(startRow).getCell(2);
//                        cell3.setCellValue("年龄" + i + j);
//                        Cell cell4 = sheet.getRow(startRow + j).getCell(3);
//                        cell4.setCellValue("动态列2" + j);
//                        Row row = sheet.getRow(startRow);
//                        Cell cell5 = row.getCell(4);
//                        if (cell5 == null) {
//                            cell5 = row.createCell(4);
//                        }
//                        cell5.setCellValue("资产" + j);
//                    }
//                    length = length + orderDetailList.size();
//                    startRow = startRow + orderDetailList.size();
//                }
//            }
//
//            out = new FileOutputStream("C:/Users/Administrator/Desktop/test.xls");
//            workbook.write(out);
//            out.close();
//            System.out.println(tpWorkbook);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//}
