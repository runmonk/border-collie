package com.runmonk.core.study.classloader;

/**
 * 类加载器 与类初始化深度剖析
 *
 * @author ：runmonk
 * @date ：Created in 2019/11/27 17:20
 */
public class MyTest12 {
    public static void main(String[] args) throws Exception {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();//获取系统类加载器


        /**
         * 调用ClassLoader类的loadClass方法加载一个类，并不是对类的主动使用 不会导致类的初始化
         */
        Class<?> clazz = classLoader.loadClass("com.runmonk.core.study.classloader.CL");//不会主动使用类
        System.out.println(clazz);

        System.out.println("==========================");

        clazz = Class.forName("com.runmonk.core.study.classloader.CL");//反射会主动使用类 并初始化

        System.out.println(clazz);

    }
}

class CL {
    static {
        System.out.println("class CL");
    }
}
