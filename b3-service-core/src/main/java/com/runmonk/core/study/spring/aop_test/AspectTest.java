//package com.runmonk.serviceadmin.spring.aop_test;
//
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.context.annotation.EnableAspectJAutoProxy;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.PostConstruct;
//
///**
// * @author ：runmonk
// * @date ：Created in 2021/4/27 17:20
// */
//
//@Aspect
//@Component
//public class AspectTest {
//    public AspectTest() {
//        System.out.println("实例化切面类");
//    }
//
//    @PostConstruct
//    public void init() {
//        System.out.println("切面类初始化");
//    }
//
//    /**
//     * 定义一个切点Pointcut 和连接点 execution
//     */
//    @Pointcut("execution(* com.runmonk.serviceadmin.spring.aop_test.service..*.*(..))")
//    public void pointCut() {
//        System.out.println("这是一个切点");
//    }
//
//    @Before("pointCut()")
//    public void advice() {
//        System.out.println("在切点之前执行");
//    }
//
//
//}
