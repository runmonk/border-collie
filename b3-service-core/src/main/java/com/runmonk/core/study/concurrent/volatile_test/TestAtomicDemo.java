package com.runmonk.core.study.concurrent.volatile_test;


/**
 * 测试volatile 无法保证原子操作
 */
public class TestAtomicDemo {

    public static void main(String[] args) {

        AtomicDemo ad = new AtomicDemo();
        for (int i = 0; i < 10; i++) {
            new Thread(ad).start();
        }
    }

}

class AtomicDemo implements Runnable {
    // 这里使用volatile修饰
    private volatile int serialNum = 0;

    @Override
    public void run() {

        try {
            Thread.sleep(2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName() + " : " + getSerialNum());

    }

    public int getSerialNum() {
        // 这里 serialNum++
        return serialNum++;
    }
}

/**
 * 由于volatile对于多线程不具有互斥性，不能保证变量的原子操作，故而如线程一读取并修改serialNum时，线程二、线程三也可能要修改！
 */