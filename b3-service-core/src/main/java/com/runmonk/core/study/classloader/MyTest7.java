package com.runmonk.core.study.classloader;

/**
 * 类加载器 双亲委派机制详解
 *
 * @author ：runmonk
 * @date ：Created in 2019/11/27 17:20
 */
public class MyTest7 {
    public static void main(String[] args) throws Exception {
        Class<?> clazz = Class.forName("java.lang.String");
        System.out.println(clazz.getClassLoader());//返回null 表示该类是由 根类加载器 加载的

        Class<?> clazz2 = Class.forName("com.runmonk.core.study.classloader.C");
        System.out.println(clazz2.getClassLoader());//系统类加载器 加载的
    }


}

class C {

}
