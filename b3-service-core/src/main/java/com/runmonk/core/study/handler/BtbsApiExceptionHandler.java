package com.runmonk.core.study.handler;

import com.runmonk.core.dto.ApiResponse;
import com.runmonk.core.exception.NoLoginException;
import com.runmonk.core.exception.RemotingAddrExcetion;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


/**
 * Created on 2017/9/9.
 *
 * @author evan.shen
 * @since
 */
@ControllerAdvice
public class BtbsApiExceptionHandler extends ApiExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(BtbsApiExceptionHandler.class);

    @ExceptionHandler(value = RemotingAddrExcetion.class)
    public Object handleRemotingAddrExcetion(RemotingAddrExcetion ex, HttpServletRequest request) {
        LOGGER.warn(ex.getMessage());

        ApiResponse res = ApiResponse.create();

        NoLoginException noLoginException = new NoLoginException();
        res.setCode(noLoginException.getCode());
        res.setMsg(ex.getMessage());
        ResponseEntity<ApiResponse> responseEntity = new ResponseEntity<ApiResponse>(res, HttpStatus.OK);
        return responseEntity;
    }
}
