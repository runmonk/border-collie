package com.runmonk.core.study.classloader;

/**
 * 类加载器与类初始化深度剖析
 *
 * @author ：runmonk
 * @date ：Created in 2019/11/27 17:20
 */
public class MyTest8 {

    public static void main(String[] args) {
        System.out.println(FinalTest.x);
//        System.out.println(FinalTest.y);  FinalTest Static block  会输出--->>> 运行期编译
    }
}

class FinalTest {
    public static final int x = 3;
//     public static  final int y=new Random().nextInt(3);

    static {
        System.out.println("FinalTest static block");
    }
}