package com.runmonk.core.study.encryption;

/**
 * 非对称加密算法DH
 *
 * @author ：runmonk
 * @date ：Created in 2019/11/12 15:50
 */
public class DHTest {
    public static void main(String[] args) {
        Integer a=1;
        Integer b=2;
        Integer c=3;
        Integer d=3;
        Integer e= 321;
        Integer f=321;
        Long g=3l;
        System.out.println(c==d);
        System.out.println(e==f);
        System.out.println(c==(a+b));
        System.out.println(c.equals(a+b));
        System.out.println(g==(a+b));
        System.out.println("2222"+g.equals(a+b));
    }

}
