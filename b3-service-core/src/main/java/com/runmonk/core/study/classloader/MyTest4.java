package com.runmonk.core.study.classloader;

import java.util.UUID;

/**
 * @name：MyTest3
 * @since：2019/11/25 10:22 PM
 * @author：runmonk
 */
public class MyTest4 {
    public static void main(String[] args)
    {
        MyParent4 myParent4 =new MyParent4();
    }
}

class  MyParent4{
    public static final String str= UUID.randomUUID().toString();

    static {
        System.out.println("MyParent4 static code");
    }
}