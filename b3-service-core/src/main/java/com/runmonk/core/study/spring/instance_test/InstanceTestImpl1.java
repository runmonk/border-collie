package com.runmonk.core.study.spring.instance_test;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * @author ：runmonk
 * @date ：Created in 2021/4/25 9:47
 */
@Component
@Primary
public class InstanceTestImpl1 implements InstanceTest {
    public InstanceTestImpl1() {
        System.out.println(1111);
    }

    @Override
    public void test() {
        System.out.println("方法" + 1111);
    }
}
