package com.runmonk.core.study.concurrent.thread_test;


import java.util.concurrent.CyclicBarrier;

/**
 * CyclicBarrier 高级构造函数：CyclicBarrier（int parties ， Runnable barrier-Action）
 * <p>
 * 用于线程到达屏障是，优先执行某个线程
 */
public class CyclicBarrierTest1 {
    static CyclicBarrier cyclicBarrier = new CyclicBarrier(2, new A());

    public static void main(String[] args) {

        //线程1
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    cyclicBarrier.await();
                    System.out.println("线程1。。。。。。。");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread1.start();

        try {
            cyclicBarrier.await();
            thread1.join();


        } catch (Exception e) {
            System.out.println("线程是否被中断" + cyclicBarrier.isBroken());
            e.printStackTrace();
        }
        System.out.println("线程阻塞的数量" + cyclicBarrier.getNumberWaiting());
        System.out.println("这是主线程。。。。。");

    }


    static class A implements Runnable {
        @Override
        public void run() {
            System.out.println("这是优先执行的线程A。。。。。。。。。");
        }
    }
}


