package com.runmonk.core.study.classloader;

/**
 * 类加载与初始化深度剖析
 * @author ：runmonk
 * @date ：Created in 2019/11/27 17:20
 */
public class MyTest9 {
    static {
        System.out.println("MyTest9 static block");
    }

    public static void main(String[] args) {
        System.out.println(Child.b);
    }
}

class Parent {
    int a = 3;

    static {
        System.out.println("Parent static block");
    }
}

class Child extends Parent {
    static int b = 4;

    static {
        System.out.println("Child static block");
    }
}