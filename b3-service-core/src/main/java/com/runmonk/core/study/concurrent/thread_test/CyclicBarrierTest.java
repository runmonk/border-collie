package com.runmonk.core.study.concurrent.thread_test;


import java.util.concurrent.CyclicBarrier;

/**
 * CyclicBarrier 测试类demo
 * <p>
 * 可循环使用的屏障，他要做的事情是，让一组线程到达一个屏障是被阻塞。
 * 直到最后一个线程到达屏障时，屏障才会开门，所有屏障拦截的线程才会继续运行
 */
public class CyclicBarrierTest {

    //设置等待屏障线程为3
    static CyclicBarrier cyclicBarrier = new CyclicBarrier(3);

    public static void main(String[] args) {

        //线程1
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    cyclicBarrier.await();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println("线程1。。。。。。。");
            }
        }).start();

        //线程2（注释掉下面线程2线程，主线程因为没有第三个线程到达屏障 连个鬼线程都不会执行）
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    cyclicBarrier.await();
                    Thread.sleep(3000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                System.out.println("线程2。。。。。。。。。。");
            }
        });
        thread2.start();
        thread2.interrupt();//中断线程

        //主线程
        try {
            cyclicBarrier.await();
        } catch (Exception e) {
            System.out.println("线程是否被中断" + cyclicBarrier.isBroken());
            e.printStackTrace();
        }

        System.out.println("主线程。。。。。。。。");
    }
}
