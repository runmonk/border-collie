package com.runmonk.core.study.concurrent.volatile_test;


/**
 * @author  runmonk
 * @since  2019-09-10
 *
 * 测试volatile的可见性问题
 */
public class TestVolatile2 {
    /**
     * 主线程
     * @param args
     */
    public static void main(String[] args){

   ThreadDemo2 td=new ThreadDemo2();
        new Thread(td).start();
        while (true){
            if (td.isFlag()){
                System.out.println("----------------------");
                break;
            }
        }
    }
}

/**
 * 内部类实现Runable接口
 */
class ThreadDemo2 implements  Runnable{

    //使用volatile 保证内存可见性

    /**
     * Java提供了一种稍弱的同步机制，即volatile 变量，用来确保将变量的更新操作通知到其他线程。你可以将volatile 看做一个轻量级的锁
     */
    private volatile boolean flag = false;

    @Override
    public void run() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        flag=true;
        System.out.println("flag= "+flag);

    }
    public boolean isFlag(){
        return this.flag;
    }

}

/**
 * 如上所说，volatile是一种轻量级的锁，synchronized是重量级锁(悲观锁)，volatile与synchronized区别如下：
 *
 *    1、 对于多线程，不是一种互斥关系；
 *     synchronized是保证互斥的，即拿到锁的线程可以执行，其他线程过来只能等待；volatile不能保证这种关系。
 *
 *    2、 不能保证变量状态的“原子性操作”。
 *
 *     3、不要将volatile用在getAndOperate场合（这种场合不原子，需要再加锁），仅仅set或者get的场景是适合volatile的。
 *
 */
