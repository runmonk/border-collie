package com.runmonk.core.study.classloader;

/**
 * 当一个接口在初始化时，并不要求其父接口都完成了初始化
 * <p>
 * 只有在真正使用到父类接口的时候（如引用接口中所定义的常量时），才会初始化
 *
 * @author ：runmonk
 * @date ：Created in 2019/11/27 17:20
 */
public class MyTest5 {
    public static void main(String[] args) {
        System.out.println(MyChild5.b);//加载父类接口但是不会初始化
        System.out.println(MyChild6.b);//不会初始化父类接口
    }
}

interface MyParent5 {
    Thread thread = new Thread() {
        {
            System.out.println("MyParents invoked");
        }
    };
}

interface MyParent6 {
    Thread thread = new Thread() {
        {
            System.out.println("MyParents invoked");
        }
    };
}

class MyChild5 implements MyParent5 {
    public static int b = 5;
}

interface MyChild6 extends MyParent6 {
    int b = 6;
}

