package com.runmonk.core.study.spring.aop_test;

import com.runmonk.core.study.spring.aop_test.service.FatherService;
import com.runmonk.core.study.spring.aop_test.service.FatherServiceImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author ：runmonk
 * @date ：Created in 2021/4/27 17:16
 */
public class Test {
    public static void main(String[] args) {
        //获取一个容器
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        FatherService fatherService = applicationContext.getBean("fatherService", FatherService.class);

        System.out.println(fatherService instanceof FatherServiceImpl);
        fatherService.say();
    }
}
