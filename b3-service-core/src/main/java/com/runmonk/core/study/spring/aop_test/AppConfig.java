package com.runmonk.core.study.spring.aop_test;

import com.runmonk.core.study.spring.aop_test.service.FatherServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;

/**
 * @author ：runmonk
 * @date ：Created in 2021/4/25 12:06
 */
@ComponentScan
@EnableAspectJAutoProxy
public class AppConfig {

    @Bean
    @Primary
    public FatherServiceImpl fatherService() {
        System.out.println("获取的第一个bean");
        return new FatherServiceImpl();
    }

}
