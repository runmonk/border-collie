package com.runmonk.core.study.concurrent.volatile_test;

import java.util.concurrent.atomic.AtomicInteger;

public class TestAtomicDemo2 {
    public static void main(String[] args) {

        AtomicDemo2 ad = new AtomicDemo2();
        for (int i = 0; i < 10; i++) {
            new Thread(ad).start();
        }
    }
}

class AtomicDemo2 implements Runnable {

//    private volatile int serialNum = 0;

    private AtomicInteger ai = new AtomicInteger();

    @Override
    public void run() {

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName() + " : " + getSerialNum());
        System.out.println("我今天新买了一个显示器！！！");

    }

    public int getSerialNum() {
//        return serialNum++;
        return ai.getAndIncrement();
    }
}