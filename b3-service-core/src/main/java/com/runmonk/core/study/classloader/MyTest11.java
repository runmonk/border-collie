package com.runmonk.core.study.classloader;

/**
 * 类加载器 与类初始化深度剖析
 *
 * @author ：runmonk
 * @date ：Created in 2019/11/27 17:20
 */
public class MyTest11 {
    public static void main(String[] args) {
        System.out.println(Child3.a);
        Child3.doSomething();
    }
}

class Parent3 {
    static int a = 3;

    static {
        System.out.println("Parent3 static block");
    }

    static void doSomething() {
        System.out.println("do something");
    }

}

class Child3 extends Parent3 {
    static {
        System.out.println("Child3 static block");
    }
}
