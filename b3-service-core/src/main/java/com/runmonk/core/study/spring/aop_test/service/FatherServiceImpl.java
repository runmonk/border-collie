package com.runmonk.core.study.spring.aop_test.service;

/**
 * @author ：runmonk
 * @date ：Created in 2021/4/27 17:12
 */
public class FatherServiceImpl implements FatherService {
    @Override
    public void say() {
        System.out.println("我是父亲");
    }

}
