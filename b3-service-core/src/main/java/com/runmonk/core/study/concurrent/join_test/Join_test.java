package com.runmonk.core.study.concurrent.join_test;


/**
 * join()的使用测试类demo
 * 测试father线程中调用son线程的join()方法，
 * son线程会在father执行前去执行，
 * 在main线程中调用father的join方法让main线程等待father执行
 *
 *
 *
 * join()方法总结：
 * 1、join()方法可以让并行执行的线程串行；
 * 2、在线程A里面调用了线程B.join方法，则接下来线程B会抢先在线程A面前执行，等到线程B全部执行完后才继续执行线程A
 * 3、join()--->>等待这个线程死亡  join(long millis)--->>等待这个线程多少秒死亡
 *
 */
public class Join_test {
    public static void main(String[] args) throws InterruptedException {

        Thread father = new Thread(new Father());  //开启一个父亲线程
        father.start();
        father.join();//让主线程等待father执行


        //主线程开始执行
        for (int i = 0; i < 10; i++) {
            System.out.println("主线程开始执行--------------------->>>>>>>>>>>" + i);
        }


    }
}

/**
 * 线程1 father
 */
class Father implements Runnable {

    @Override
    public void run() {
        System.out.println("老爸要抽烟,发现没烟了,给了100块让儿子去买中华......");
        Thread son = new Thread(new Son());   //让儿子去买烟
        son.start();    //开启儿子线程后，儿子线程进入就绪状态等待CPU调度，不一定立即执行儿子线程，所以可能会出现儿子没把烟买回来老爸就有烟抽了

        try {
            son.join(); //让儿子先插队去买烟
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("儿子不见了,报警寻找");
        }

        System.out.println("老爸接过烟,把零钱给了儿子");
        System.out.println("儿子很开心,出门去了游戏厅");
    }
}


/**
 * 线程2 son
 */
class Son implements Runnable {

    @Override
    public void run() {
        System.out.println("儿子接过钱蹦跶出了门");
        System.out.println("路过游戏厅玩了10秒钟");
        for (int i = 1; i <= 10; i++) {
            System.out.println(i + "秒");
            try {
                Thread.sleep(1000); //此时休眠可能会让其他线程进行,出错率增加,通过join方法解决
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("赶紧去买烟");
        System.out.println("回家把烟给老爸");
    }
}