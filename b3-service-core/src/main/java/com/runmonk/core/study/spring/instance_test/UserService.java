//package com.runmonk.core.study.spring.instance_test;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// * @author ：runmonk
// * @date ：Created in 2021/4/25 12:24
// */
//@Component
//public class UserService {
//    @Autowired
//    private InstanceTest instanceTest;
//
//    public UserService() {
//    }
//
//    public void say() {
//        instanceTest.test();
//    }
//}
