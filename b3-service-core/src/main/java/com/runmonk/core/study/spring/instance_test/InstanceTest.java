package com.runmonk.core.study.spring.instance_test;

/**
 * @author ：runmonk
 * @date ：Created in 2021/4/25 9:46
 */
public interface InstanceTest {
    void test();
}
