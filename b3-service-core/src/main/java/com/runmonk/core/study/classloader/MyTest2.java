package com.runmonk.core.study.classloader;

/**
 * 常量在编译阶段会存入到调用这个常量方法所在的类的常量池中，
 * 本质上，调用类并没有直接引用到定义常量类，因此并不会触发
 *
 *
 *
 * 助记符：
 * ldc 表示将int、float或是String类型的常量从常量池推送至栈顶
 * bipush 表示将单字节（-128-127）的常量推送至栈顶
 * sipush 表示将短整型（-32768-32767）
 *
 * @name：MyTest3
 * @since：2019/11/25 10:22 PM
 */
public class MyTest2 {
    public static void main(String[] args) {

        System.out.println(MyParent2.str);
    }
}

class MyParent2 {
    public static final String str = "hello world";

    public  static  final  short s=7;
    public static  final  int i=127;

    static {
        System.out.println("MyParent2 static block");
    }
}

