package com.runmonk.core.study.spring.instance_test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;

/**
 * @author ：runmonk
 * @date ：Created in 2021/4/25 12:06
 */
@ComponentScan
public class AppConfig {

    @Bean
    @Primary
    public InstanceTest instanceTestImpl1() {
        System.out.println("获取的第一个bean");
        return new InstanceTestImpl1();
    }

    @Bean
    public InstanceTest instanceTestImpl2() {
        System.out.println("获取的第二个bean");
        return new InstanceTestImpl2();
    }

//    @Bean
//    public UserService userService() {
//        System.out.println("实例化userService");
//        return new UserService();
//    }


}
