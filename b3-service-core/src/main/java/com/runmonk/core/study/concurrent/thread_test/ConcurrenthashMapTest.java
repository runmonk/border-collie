package com.runmonk.core.study.concurrent.thread_test;


import java.util.HashMap;
import java.util.UUID;

/**
 * ConcurrenthashMap（jdk1.8） 测试类
 * <p>
 * 学习要点：
 * <p>
 * 1、CAS原理
 * <p>
 * 2、ConcurrentHashMap的数据结构
 * <p>
 * 3、ConcurrentHashMap的初始化
 * <p>
 * 4、Node链表喝红黑树结构装换
 * <p>
 * 5、ConcurrentHashMap的操作（get、put、size）
 */
public class ConcurrenthashMapTest {

    public static void main(String[] args) {

    }

}


/**
 * 测试hashMap在多线程的环境下 会出现死锁的情况
 */
class HashMapTest {


    public static void main(String[] args) throws InterruptedException {

        final HashMap<String, String> map = new HashMap<String, String>(2);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            map.put(UUID.randomUUID().toString(), "");
                        }
                    },"ftf"+i).start();
                    System.out.println(Thread.currentThread());
                }
            }
        },"ftf");

        thread.start();
        thread.join();

    }
}
