package com.runmonk.core.study.spring.instance_test;

import org.springframework.stereotype.Component;

/**
 * @author ：runmonk
 * @date ：Created in 2021/4/25 9:47
 */
@Component
public class InstanceTestImpl2 implements InstanceTest {
    public InstanceTestImpl2() {
        System.out.println(22222);
    }

    @Override
    public void test() {
        System.out.println("方法"+22222);
    }
}
