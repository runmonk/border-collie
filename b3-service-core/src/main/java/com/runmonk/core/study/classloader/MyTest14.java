package com.runmonk.core.study.classloader;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

/**
 * 通过类加载器 获取字节码文件的资源路径
 *
 * @author ：runmonk
 * @date ：Created in 2019/11/27 17:20
 */
public class MyTest14 {
    public static void main(String[] args) throws IOException {
        //获取当前线程的类加载器
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        String resourceName = "com/runmonk/core/study/classloader/MyTest13.class";
        Enumeration<URL> urls = classLoader.getResources(resourceName);

        while (urls.hasMoreElements()) {
            URL url = urls.nextElement();
            System.out.println(url);
        }

        System.out.println("=================");
        //获取当前类的 class对象
        Class<?> clazz = MyTest14.class;
        ClassLoader classLoader1 = clazz.getClassLoader();
        System.out.println(classLoader1);//由应用类系统类 加载器加载

        Class<?> stringClazz = String.class;
        ClassLoader stringClassLoader = stringClazz.getClassLoader();
        System.out.println(stringClassLoader);//rt.jar 由根类加载器 加载

        //获取系统类加载器
        ClassLoader classLoader2 = ClassLoader.getSystemClassLoader();
        System.out.println(classLoader2);


    }
}
