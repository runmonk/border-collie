package com.runmonk.core.study.spring.instance_test;

import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;


/**
 * @author ：runmonk
 * @date ：Created in 2021/4/25 9:43
 */
@Component
public class spring {
    @Resource(name="instanceTestImpl1")
    private InstanceTest instanceTest;

    public spring() {
        System.out.println("spring");
    }

    /**
     * 说话
     */
    public void say() {
        instanceTest.test();
        System.out.println("调用方法");
    }

    public static void main(String[] args) {
        spring spring = new spring();
        spring.say();
    }
}
