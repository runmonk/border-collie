package com.runmonk.core.study.java_8_new_features;


import java.util.function.Supplier;

/**
 * java8新特性
 * <p>
 * 接口的默认方法和静态方法
 */
public class DefaulableTest {


    public static void main( String[] args ) {
        Defaulable defaulable = DefaulableFactory.create( DefaultableImpl::new );
        System.out.println( defaulable.notRequired() );

        defaulable = DefaulableFactory.create( OverridableImpl::new );
        System.out.println( defaulable.notRequired() );
    }

    /**
     * demo1
     * 接口中可以定义default 默认方法，子类可以选择去重写或者不重写
     */
    private static class DefaultableImpl implements Defaulable {
    }

    private static class OverridableImpl implements Defaulable {
        @Override
        public String notRequired() {
            return "Overridden implementation";
        }
    }

    private interface DefaulableFactory {
        // Interfaces now allow static methods
        static Defaulable create(Supplier<Defaulable> supplier) {
            return supplier.get();
        }
    }

    /**
     * 接口1   可以多继承 类可以多实现
     */
    private interface Defaulable extends Defaulable2, Defaulable3 {

        default String notRequired() {
            return "Default implementation1";
        }
    }

    /**
     * 接口2
     */
    private interface Defaulable2 {
        default String notRequired() {
            return "Default implementation2";
        }
    }

    /**
     * 接口3
     */
    private interface Defaulable3 {
        default String notRequired() {
            return "Default implementation3";
        }
    }

}





