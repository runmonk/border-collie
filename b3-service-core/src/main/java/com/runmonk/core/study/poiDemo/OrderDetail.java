package com.runmonk.core.study.poiDemo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author ：runmonk
 * @date ：Created in 2019/12/4 9:30
 */
public class OrderDetail {

    private BigDecimal money;

    private Date time;

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
