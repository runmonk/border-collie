package com.runmonk.core.study.classloader;

/**
 * @name：MyTest3
 * @since：2019/11/25 10:22 PM
 * @author：runmonk
 *
 * 对于静态字段来说，只是直接定义了该字段的类才会被初始化；
 * 当一个类在初始化时，要求其父类信息全部加载完毕。
 *
 *
 *
 * -XX:+TraceClassLoading 用于追踪类的加载信息并打印出来
 *
 * -XX:+<option>, 表示开始option选项
 * -XX:-<option>, 表示关闭option选项
 */
public class MyTest1 {
    public static void main(String[] args) {
        System.out.println(MyParent1.str);
    }
}

class  MyParent1{
    public static  String str= "hello world";

    static {
        System.out.println("MyParent1 static block");
    }
}

class MyChild1 extends MyParent1{
    public static  String str2="welcome";

}