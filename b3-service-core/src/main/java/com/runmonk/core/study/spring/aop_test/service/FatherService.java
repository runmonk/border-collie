package com.runmonk.core.study.spring.aop_test.service;

/**
 * @author ：runmonk
 * @date ：Created in 2021/4/27 17:59
 */
public interface FatherService {
    public void say();
}
