package com.runmonk.core.util;


import jakarta.servlet.http.HttpServletRequest;

/**
 * @author evan.shen
 * @since 2018/6/6
 */
public class RequestLocalUtil {
    private static ThreadLocal<HttpServletRequest> threadLocal = new ThreadLocal<>();

    public static HttpServletRequest get() {
        return threadLocal.get();
    }

    public static void put(HttpServletRequest request) {
        threadLocal.set(request);
    }

    public static void remove() {
        threadLocal.remove();
    }
}
