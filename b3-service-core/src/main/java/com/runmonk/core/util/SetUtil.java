package com.runmonk.core.util;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created on 2017/8/23.
 *
 * @author evan.shen
 */
public class SetUtil {
    public static void addAllInteger(Set<Integer> set, String... args) {

        for (Serializable e : args) {
            if (e != null) {
                set.add(Integer.valueOf(StringUtils.trim(e + "")));
            }
        }
    }

    public static void addAllInteger(Set<Integer> set, Integer... args) {
        if (args == null) {
            return;
        }

        for (Integer e : args) {
            if (e != null) {
                set.add(e);
            }
        }
    }

    public static void addAllLong(Set<Long> set, String... args) {
        for (String e : args) {
            if (StringUtils.isNotBlank(e)) {
                set.add(Long.valueOf(StringUtils.trim(e + "")));
            }
        }
    }

    public static void addAllString(Set<String> set, String... args) {
        for (Serializable e : args) {
            if (e != null) {
                set.add(StringUtils.trim(e + ""));
            }
        }
    }

    public static void addLong(Set<Long> set, Long arg) {
        if (arg != null) {
            set.add(arg);
        }
    }

    public static Integer[] sortFullpath(String fullpath, String regex) {
        if (StringUtils.isEmpty(fullpath)) {
            return new Integer[0];
        }
        String[] path = fullpath.split(regex);
        Set<Integer> pathSet = new TreeSet<>();
        for (String p : path) {
            if (StringUtils.isEmpty(p)) {
                continue;
            }
            pathSet.add(Integer.valueOf(p));
        }
        return pathSet.toArray(new Integer[pathSet.size()]);
    }

    /**
     * 将完整的String 根据正则匹配拆分成Long类型的Set
     *
     * @param fullPath
     * @param regex
     */
    public static Set<Long> splitFullPathToLongSet(String fullPath, String regex) {
        if (StringUtils.isEmpty(fullPath)) {
            return new HashSet<>(0);
        }
        String[] path = fullPath.split(regex);
        Set<Long> pathSet = new TreeSet<>();
        for (String p : path) {
            if (StringUtils.isEmpty(p)) {
                continue;
            }
            pathSet.add(Long.valueOf(p));
        }
        return pathSet;
    }

    public static void addLongAllSet(Set<Long> target, Set<Long> source) {
        if (CollectionUtils.isEmpty(target) || CollectionUtils.isEmpty(source)) {
            return;
        }
        target.addAll(source);
    }

    public static void addLongAllSet(Set<Long> target, Long... source) {
        if(target == null){
            return;
        }
        if (ArrayUtils.isEmpty(source)) {
            return;
        }
        for (Long each : source) {
            if (null != each) {
                target.add(each);
            }
        }
    }

    public static void addIntegerAllSet(Set<Integer> target, Set<Integer> source) {
        if (target == null) {
            return;
        }
        if (CollectionUtils.isEmpty(source)) {
            return;
        }
        target.addAll(source);
    }

    public static void addLongAllList(Set<Long> target, List<Long> source) {
        if (target == null) {
            return;
        }
        if (CollectionUtils.isEmpty(source)) {
            return;
        }
        target.addAll(source);
    }

    public static String toString(Set<Integer> set, String separator) {
        if (CollectionUtils.isEmpty(set)) {
            return StringUtils.EMPTY;
        }
        return StringUtils.join(set.toArray(), ",");
    }

    public static void removeLong(Set<Long> set, Long source) {
        if (CollectionUtils.isEmpty(set)) {
            return;
        }
        if(source == null){
            return;
        }
        set.remove(source);
    }


    public static Set<Long> generateLongSet(Long arg) {
        Set<Long> set = new HashSet<>();
        if(arg == null){
            return set;
        }
        set.add(arg);
        return set;
    }

    /**
     * 转换为 long
     * @param integers
     * @return
     */
    public static Set<Long> toLong(Set<Integer> integers) {
        if (CollectionUtil.isEmpty(integers)) {
            return new HashSet<>();
        }
        Set<Long> longList = new HashSet<>();
        for (Integer integer : integers) {
            longList.add(Long.valueOf(integer));
        }
        return longList;
    }
}
