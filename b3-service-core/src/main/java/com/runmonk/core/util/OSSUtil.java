package com.runmonk.core.util;

import org.apache.commons.lang3.StringUtils;

/**
 *
 */
public class OSSUtil {
    /**
     * 获取 ossPath
     *
     * @param ossUrl
     * @param url
     * @return
     */
    public static String getOssPath(String ossUrl, String url) {
        if (StringUtils.isBlank(ossUrl) || StringUtils.isBlank(url)) {
            return url;
        }
        if (!StringUtils.endsWith(ossUrl, "/")) {
            ossUrl = ossUrl + "/";
        }
        String[] array = url.split(ossUrl);
        return array[array.length - 1];
    }

}
