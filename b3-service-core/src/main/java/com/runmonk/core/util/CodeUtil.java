package com.runmonk.core.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CodeUtil {

    private static final Map<Integer, String> zeroMap = new ConcurrentHashMap<>();

    static {
        zeroMap.put(1, "0");
        zeroMap.put(2, "00");
        zeroMap.put(3, "000");
        zeroMap.put(4, "0000");
        zeroMap.put(5, "00000");
        zeroMap.put(6, "000000");
    }

    public static String addOne(String code) {
        StringBuffer buffer = new StringBuffer(code);
        Integer codeInt = Integer.valueOf(code) + 1;
        buffer.replace(code.length() - String.valueOf(codeInt).length(), code.length(), String.valueOf(codeInt));
        return buffer.toString();
    }

    public static String getNextCode(int count, int size) {
        String code = zeroMap.get(size);
        StringBuffer buffer = new StringBuffer(code);
        return buffer.replace(code.length() - String.valueOf(count + 1).length(), code.length(), String.valueOf(count + 1)).toString();
    }
    public static String getCode(int size){
        String code = zeroMap.get(size);
        return code;
    }
}
