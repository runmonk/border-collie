//package com.runmonk.core.util;
//
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.lang.reflect.Method;
//
///**
// * Aop工具类
// */
//public class AopUtil {
//    private static final Logger LOGGER = LoggerFactory.getLogger(AopUtil.class);
//
//
//    /**
//     * 获取当前执行的方法
//     *
//     * @param joinPoint 连接点
//     * @return 方法
//     */
//    public static Method getMethod(ProceedingJoinPoint joinPoint) {
////        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
////        Method method = signature.getMethod();
////        return method;
//
//        String methodName = joinPoint.getSignature().getName();
//        Method[] methods = joinPoint.getTarget().getClass().getMethods();
//        Method resultMethod = null;
//        for (Method method : methods) {
//            if (method.getName().equals(methodName)) {
//                resultMethod = method;
//                break;
//            }
//        }
//        return resultMethod;
//    }
//
//    /**
//     * @param clazz
//     * @param method
//     * @return
//     */
//    public static String getMethodFullname(Class clazz, Method method) {
//        return clazz.getCanonicalName() + "." + method.getName();
//    }
//
//    /**
//     * @param joinPoint
//     * @return
//     */
//    public static String getMethodFullname(ProceedingJoinPoint joinPoint) {
//        String canonicalName = joinPoint.getTarget().getClass().getCanonicalName();
//        String methodName = joinPoint.getSignature().getName();
//        return canonicalName + "." + methodName;
//    }
//
//}
