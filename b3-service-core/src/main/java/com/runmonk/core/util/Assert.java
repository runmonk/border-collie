package com.runmonk.core.util;

import java.math.BigDecimal;

public class Assert extends org.springframework.util.Assert {

    /**
     * 数字区间
     */
    public static void integerIntervalEnd(Integer source, String errorMsg, Integer end) {
        integerInterval(source,errorMsg,null,end);
    }

    /**
     * 数字区间
     */
    public static void integerIntervalStart(Integer source, String errorMsg, Integer start) {
        integerInterval(source,errorMsg,start,null);
    }

    /**
     * 数字区间
     */
    public static void integerInterval(Integer source, String errorMsg, Integer start,Integer end) {
        Assert.notNull(source,errorMsg);
        if(start == null && end == null){
            return;
        }
        if (start != null && end != null) {
            if (source > end || source < start){
                throw new IllegalArgumentException(errorMsg);
            }
        } else if(end != null){
            if (source > end ){
                throw new IllegalArgumentException(errorMsg);
            }
        } else if(start != null){
            if (source < start) {
                throw new IllegalArgumentException(errorMsg);
            }
        }
        return;
    }

    /**
     * decimal 区间
     */
    public static void decimalEqStart(BigDecimal source, String errorMsg, BigDecimal start) {
        decimalEqInterval(source, errorMsg, start,null);
    }

    /**
     * decimal 区间
     */
    public static void decimalEqEnd(BigDecimal source, String errorMsg,BigDecimal end) {
        decimalEqInterval(source, errorMsg, null, end);
    }

    /**
     * decimal 区间
     */
    public static void decimalEqInterval(BigDecimal source, String errorMsg, BigDecimal start, BigDecimal end) {
        Assert.notNull(source, errorMsg);
        if (start == null && end == null) {
            return;
        }
        if (start != null && end != null) {
            if (source.compareTo(start) < 0 || source.compareTo(end) > 0) {
                throw new IllegalArgumentException(errorMsg);
            }
        } else if (end != null) {
            if (source.compareTo(end) < 0) {
                throw new IllegalArgumentException(errorMsg);
            }
        } else if (start != null) {
            if (source.compareTo(start) < 0) {
                throw new IllegalArgumentException(errorMsg);
            }
        }
        return;
    }

    /**
     * decimal 区间
     */
    public static void decimalEnd(BigDecimal source, String errorMsg, BigDecimal end) {
        decimalInterval(source,errorMsg,null,end);
    }

    /**
     * decimal 区间
     */
    public static void decimalStart(BigDecimal source, String errorMsg, BigDecimal start) {
        decimalInterval(source,errorMsg,start,null);
    }

    /**
     * decimal 区间
     */
    public static void decimalInterval(BigDecimal source, String errorMsg, BigDecimal start, BigDecimal end) {
        Assert.notNull(source, errorMsg);
        if (start == null && end == null) {
            return;
        }
        if (start != null && end != null) {
            if (source.compareTo(start) <= 0 || source.compareTo(end) >= 0) {
                throw new IllegalArgumentException(errorMsg);
            }
        } else if (end != null) {
            if (source.compareTo(end) < 1) {
                throw new IllegalArgumentException(errorMsg);
            }
        } else if (start != null) {
            if (source.compareTo(start) < 1) {
                throw new IllegalArgumentException(errorMsg);
            }
        }
        return;
    }

    /**
     * 数字区间
     */
    public static void decimalEq(BigDecimal source, String errorMsg,BigDecimal target) {
        Assert.notNull(source, errorMsg);
        if(target == null){
            return;
        }
        if(source.compareTo(target) != 0){
            throw new IllegalArgumentException(errorMsg);
        }
    }

    public static void main(String[] args) {
        decimalEqInterval(new BigDecimal(0),"decimalInterval.error",new BigDecimal(0),new BigDecimal(100));
    }
}
