package com.runmonk.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * byte util
 *
 * @author mouhaining
 * @date 2018-06-08
 */
public class ByteUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ByteUtil.class);

    /**
     * input 流 转换为byte 数组
     * @param inStream
     * @return
     */
    public static final byte[] input2byte(InputStream inStream) {
        try {
            ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
            byte[] buff = new byte[100];
            int rc = 0;
            while ((rc = inStream.read(buff, 0, 100)) > 0) {
                swapStream.write(buff, 0, rc);
            }
            return swapStream.toByteArray();
        } catch (Exception e) {
            LOGGER.error("InputStream to ByteArray error", e);
        }
        return null;
    }
}
