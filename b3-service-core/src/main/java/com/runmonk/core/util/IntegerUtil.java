package com.runmonk.core.util;

public class IntegerUtil {

    public static boolean same(Integer source, Integer target) {
        if (source == null) {
            return false;
        }
        return source.equals(target);
    }
}
