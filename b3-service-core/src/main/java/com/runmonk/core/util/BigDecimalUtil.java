package com.runmonk.core.util;


import com.runmonk.core.constant.CommonConstant;

import java.math.BigDecimal;

public class BigDecimalUtil {

    public static final BigDecimal HUNDRED = new BigDecimal(100);

    public static BigDecimal add(BigDecimal target, BigDecimal source) {
        if (source != null) {
            target = target.add(source);
        }
        return target;
    }

    public static BigDecimal sum(BigDecimal... bigDecimals) {
        BigDecimal returnV = BigDecimal.ZERO;
        for (BigDecimal e : bigDecimals) {
            if (e != null) {
                returnV = returnV.add(e);
            }

        }
        return returnV;
    }

    public static BigDecimal subtract(BigDecimal source, BigDecimal... subs) {
        if (source == null) {
            return source;
        }
        if (subs == null || subs.length < 1) {
            return source;
        }
        for (BigDecimal sub : subs) {
            if (sub != null) {
                source = source.subtract(sub);
            }
        }
        return source;
    }

    public static BigDecimal multiply(BigDecimal source, BigDecimal target) {
        if (source == null || target == null) {
            return source;
        }
        return source.multiply(target);
    }

    public static BigDecimal add(BigDecimal target, BigDecimal... sources) {
        if (sources == null || sources.length == 0) {
            return target;
        }
        for (BigDecimal source : sources) {
            if (source != null) {
                target = target.add(source);
            }
        }
        return target;
    }

    public static BigDecimal divide(BigDecimal target, BigDecimal source) {
        return divide(target, source, CommonConstant.BIGDECIMAL_SCALE_RESULT, BigDecimal.ROUND_HALF_UP);
    }

    public static BigDecimal divide(BigDecimal target, BigDecimal source, Integer newScale, Integer roundingMode) {
        if (source != null) {
            target = target.divide(source,newScale,roundingMode);
        }

        return target;
    }


    public static BigDecimal addAndSetScale(BigDecimal target, BigDecimal source, int newScale, int roundingMode) {
        if (source != null) {
            target = target.add(source);
        }
        return target.setScale(newScale, roundingMode);
    }

    public static boolean isNotNullOrZero(BigDecimal value) {
        return !isNullOrZero(value);
    }

    public static boolean isNullOrZero(BigDecimal value) {
        if (value == null) {
            return true;
        }
        if (value.compareTo(BigDecimal.ZERO) == 0) {
            return true;
        }
        return false;
    }

    public static boolean gtZero(BigDecimal source) {
        return gt(source, BigDecimal.ZERO);
    }

    /**
     * 大于等于
     * @param source
     * @param target
     * @return
     */
    public static boolean gte(BigDecimal source, BigDecimal target) {
        if (source == null) {
            return false;
        }
        if (target == null) {
            target = BigDecimal.ZERO;
        }
        if (source.compareTo(target) > -1) {
            return true;
        }
        return false;
    }

    /**
     * 大于
     * @param source
     * @param gt
     * @return
     */
    public static boolean gt(BigDecimal source, BigDecimal gt) {
        if (source == null) {
            return false;
        }
        if (gt == null) {
            gt = BigDecimal.ZERO;
        }
        if (source.compareTo(gt) > 0) {
            return true;
        }
        return false;
    }

    public static boolean eqZero(BigDecimal source) {
        return source.compareTo(BigDecimal.ZERO) == 0;
    }

    public static boolean notEqZero(BigDecimal source) {
        return source.compareTo(BigDecimal.ZERO) == 0;
    }

    public static BigDecimal getSmall(BigDecimal b1,BigDecimal b2){
        if(b1 == null){
            return b2;
        }
        if(b2 == null){
            return b1;
        }

        return b1.compareTo(b2) >= 0 ? b2 : b1;
    }


    public static void main(String[] args) {

        BigDecimal bigDecimal = percentageConversion(BigDecimal.valueOf(32.523456789));
        System.out.println(bigDecimal);
    }

    /**
     * 百分比转化
     * 比如数据库存的是50%，经此函数转换为0.5（如果除不尽，则保留两位小数）
     * 12.58484--->0.13    1.12345884-->0.11
     *
     * @param target 目标数字
     * @return
     */
    public static BigDecimal percentageConversion(BigDecimal target) {
        if (null == target || target.compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ZERO;
        }
        BigDecimal result = target.divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_EVEN);
        return result;
    }

    /**
     *
     * @param b1
     * @param b2
     * @return
     */
    public static boolean eq(BigDecimal b1, BigDecimal b2) {
        if (null == b1 || b2 == null) {
            return false;
        }
        return b1.compareTo(b2) == 0;
    }

    /**
     * 月利率 转换为日利率
     *
     * @param rate
     */
    public static BigDecimal monthRateToDayRate(BigDecimal rate) {
        return rate.divide(BigDecimal.valueOf(CommonConstant.THOUSAND), CommonConstant.BIGDECIMAL_SCALE_TEN, BigDecimal.ROUND_HALF_UP)
                .multiply(BigDecimal.valueOf(CommonConstant.MONTH))
                .divide(BigDecimal.valueOf(CommonConstant.DAY_FOR_YEAR), CommonConstant.BIGDECIMAL_SCALE_TEN, BigDecimal.ROUND_HALF_UP);
    }
}
