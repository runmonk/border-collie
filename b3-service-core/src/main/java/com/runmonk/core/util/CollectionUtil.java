package com.runmonk.core.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CollectionUtil extends CollectionUtils {

    public static <T> List<T> arrayToListNotNull(Object... objs) {
        List list = CollectionUtils.arrayToList(objs);
        list.removeAll(Collections.singleton(null));
        return list;
    }

    public static String join(Collection<? extends Serializable> collection, String separator) {
        if (CollectionUtils.isEmpty(collection)) {
            return StringUtils.EMPTY;
        }
        return StringUtils.join(collection.toArray(), ",");
    }

}
