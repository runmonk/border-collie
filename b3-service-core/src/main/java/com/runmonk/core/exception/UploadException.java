package com.runmonk.core.exception;

import com.runmonk.core.constant.CommonOperateResult;

/**
 * @author evan.shen
 * @since 2017/12/6
 */
public class UploadException extends ServiceException {
    private static final long serialVersionUID = -951777198106425369L;

    public UploadException() {
        super(CommonOperateResult.UPLOAD_ERROR);
    }

    public UploadException(String message) {
        super(CommonOperateResult.UPLOAD_ERROR.getCode(), message);
    }

}
