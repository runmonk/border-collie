package com.runmonk.core.exception;

import com.runmonk.core.constant.CommonOperateResult;

/**
 * Created on 2017/9/5.
 *
 * @author evan.shen
 */
public class RemotingAddrExcetion extends ServiceException {
    private static final long serialVersionUID = -951777198106425369L;

    public RemotingAddrExcetion() {
        super(CommonOperateResult.REMOTING_ADDR_WRONG);
    }

    public RemotingAddrExcetion(String message) {
        super(CommonOperateResult.REMOTING_ADDR_WRONG.getCode(), message);
    }
}
