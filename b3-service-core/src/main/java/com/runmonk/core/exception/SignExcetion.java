package com.runmonk.core.exception;


import com.runmonk.core.constant.CommonOperateResult;

/**
 * Created on 2017/9/5.
 *
 * @author evan.shen
 */
public class SignExcetion extends ServiceException {
    private static final long serialVersionUID = -951777198106425369L;

    public SignExcetion() {
        super(CommonOperateResult.SIGN_WRONG);
    }

    public SignExcetion(String message) {
        super(CommonOperateResult.SIGN_WRONG.getCode(), message);
    }
}
