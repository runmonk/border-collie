package com.runmonk.core.exception;


import com.runmonk.core.constant.CommonOperateResult;

/**
 * Created on 2017/8/31.
 *
 * @author evan.shen
 */
public class DataRepeatedException extends ServiceException {
    private static final long serialVersionUID = -951777198106425369L;

    public DataRepeatedException() {
        super(CommonOperateResult.DATA_REPEATED);
    }

    public DataRepeatedException(String message) {
        super(CommonOperateResult.DATA_REPEATED.getCode(), message);
    }
}
