package com.runmonk.core.base;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 基础 domain
 *
 * @author mouhaining
 * @since 2018/11/02
 */
@Data
public abstract class AbstractDomain implements Serializable {

    public AbstractDomain(){
    }

    public AbstractDomain(Long id){
        this.id = id;
    }

    private Long id;

    private Long creatorId;//创建人id

    private String creatorName;//创建人名称

    private Date gmtCreate;//创建时间

    private Long modifierId;//修改人id

    private String modifierName;//修改人名称

    private Date gmtModify;//修改时间

    private Integer isDeleted;//



}


