package com.runmonk.core.constant;

import com.runmonk.core.dto.OperateResultType;

/**
 * 公用的返回类型，各子系统各业务模块可以继承该类
 * Created by shen.wei on 2017/4/24.
 */
public interface OperateCommonResultType {
    /**
     * 成功
     */
    OperateResultType SUCCESS = new OperateResultType("SUCCESS", "成功");
    /**
     * 系统出错
     */
    OperateResultType ERROR = new OperateResultType("ERROR", "系统出错");
    /**
     * 参数不正确
     */
    OperateResultType PARAMETER_INVALID = new OperateResultType("PARAMETER_INVALID", "参数不正确");
    /**
     * 请求地址不正确
     */
    OperateResultType HTTP_URL_INVALID = new OperateResultType("HTTP_URL_INVALID", "请求地址不正确");
    /**
     * 请求方式不正确
     */
    OperateResultType HTTP_METHOD_INVALID = new OperateResultType("HTTP_METHOD_INVALID", "请求方式不正确");
    /**
     * 请求方式不正确
     */
    OperateResultType HTTP_MEDIA_TYPE_INVALID = new OperateResultType("HTTP_MEDIA_TYPE_INVALID", "请求格式不正确");


}
