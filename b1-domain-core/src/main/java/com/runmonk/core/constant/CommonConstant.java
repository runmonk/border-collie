package com.runmonk.core.constant;


import java.math.BigDecimal;
import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * 公用常量
 * <p>
 * create at 2016年4月2日 下午1:18:47
 *
 * @author Evan.Shen
 * @since %I%, %G%
 * @see
 */
public interface CommonConstant  {

    /**
     * 新闻图片尺寸 H5
     */
    int SCALE_WIDTH_H5= 200;

    /**
     * 新闻图片尺寸 PC
     */
    int SCALE_WIDTH_PC= 1000;

    /**
     * 图片尺寸
     */
    int SCALE_WIDTH = 80;

    /**
     * 进行中
     */
    int ONGOING=0;

    /**
     * 完成
     */
    int COMPLETE=1;

    /**
     * 是
     */
    int YES = 1;
    /**
     * 否
     */
    int NO = 0;

    //物流单价单位

    /**
     * 元/吨
     */
    int TONS=2;

    /**
     * 元/车
     */
    int CAR=1;

    /**
     * 删除状态 已删除
     */
    int DELETED_TAG = 1;

    /**
     * 删除状态 未删除
     */
    int NOT_DELETED_TAG = 0;

    /**
     * 物流公司组织类型 线上
     */
    int ONLINE_ORG_TYPE = 0;

    /**
     * 物流公司组织类型 线下
     */
    int OFFLINE_ORG_TYPE = -1;


    /**
     * 状态 正常
     */
    int STATUS_NORMAL = 1;

    /**
     * 字符编码
     */
    String WEB_ENCODING = "UTF-8";

    /**
     * 分隔符
     */
    String SPLIT = "&_&";

    /**
     * 短信验证码默认有效时间
     */
    int VALIDATE_CODE_DEFAILT_EXPIRE_SECONDS = 600;

    /**
     * 小数
     */
    int BIGDECIMAL_SCALE_COMPUTING = 4;

    /**
     * 小数
     */
    int BIGDECIMAL_SCALE_SIX = 6;

    /**
     * 小数
     */
    int BIGDECIMAL_SCALE_RESULT = 2;

    /**
     * 小数
     */
    int BIGDECIMAL_SCALE_TEN = 10;

    /**
     * 小数
     */
    int BIGDECIMAL_SCALE_TWENTY = 20;

    /**
     * 默认每页记录数
     */
    int DEFAULT_PAGE_SIZE = 10;

    /**
     * 默认token，没有登录时的接口用
     */
    String DEFAULT_TOKEN = "69c735ba3d4b4778e2085f120e0fda52af67f19f";


    /**
     *  默认SECRET，没有登录时的接口用
     */
    String DEFAULT_SECRET = "1ce780e1ce4319df82fb5801a90b01f47a3d59c741c0b63e19de1d905233e1b1";


    String MESSAGE_TOPIC = "mizhi-btbs";

    /**
     * 初始密码
     */
    String INIT_PASSWORD = "123456";

    String SLASH = "/";

    //中文排序
    Comparator CHINA_COMPARE = Collator.getInstance(Locale.CHINA);

    int ZERO = 0;

    int ONE = 1;

    int HUNDRED = 100;

    int THOUSAND = 1000;

    int PASS = 1;// 确认通过/审批通过

    int TEN_THOUSAND = 100000;

    //供应方权限前缀
    String FUNCTION_SUPPLIER_PREFIX = "s-";

    //需求方权限前缀
    String FUNCTION_CUSTOM_PREFIX = "c-";

    //平台权限前缀
    String FUNCTION_PLATFORM_PREFIX = "p-";

    //物流公司权限前缀
    String FUNCTION_LOGISTICS_PREFIX = "l-";

    //pdf 文件后缀
    String PDF_SUFFIX = ".pdf";

    //临时
    String TEMP = "temp";

    String OSS_PDF_FILE_NAME = "pdf";

    String FORMAT_SHORT_STRING_CN = "yyyy年MM月dd日";

    int PLATFORM_ORG_CODE = 0;//平台组织id

    //将单位转换为万
    int THE_THOUSAND = 10000;

    BigDecimal SINOIOV_CONVER_UNIT = BigDecimal.valueOf(600000);

    //默认一个月天数
    int DAY_FOR_MONTH = 30;

    //一年月数
    int MONTH = 12;

    // 一年天数
    int DAY_FOR_YEAR = 365;
}
