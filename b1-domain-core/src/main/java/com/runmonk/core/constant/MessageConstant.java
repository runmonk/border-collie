package com.runmonk.core.constant;

/**
 * @author: quanguo.wang
 * @date: 2017/12/13 0013
 * 发送系统消息时的常量
 */
public interface MessageConstant {
    /**
     * 插入用户名
     */
    String USER_NAME_STRING_KEY = "${userName}";

    String MESSAGE_BEGIN_SYMBOL = "${";

    String MESSAGE_END_SYMBOL = "}";

    String TARGET_PATH_STRING = "targetPath";


//    String DATE_STRING_KEY = "date";
}
