package com.runmonk.core.export;

/**
 * 导出excel枚举
 *
 * @author runmonk
 * @since 2019/3/13
 */
public interface AbstractExportExcelEnum {

    /**
     * 标题名
     */
    String getTitleName();


    /**
     * sheetName
     *
     * @return
     */
    String getSheetName();


    /**
     * 文件名
     */
    String getFileName();

    /**
     * excel模板路径
     */
    String getTemplatePath();

    /**
     * 对象名称
     */
    Class getObject();

    /**
     * sheet 的位置
     * 如0,1 只导出sheet0和1的页面
     * 可以不传,默认为0
     */
    Integer[] getSheetNums();
}
